const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRouter = require('./lib/router/user-router');

const app = express();

app.use(cors());
app.use(express.json());
app.use('/', userRouter);

mongoose.connect('mongodb+srv://jpasa:nqh8iHyRKac2JBCj@user-contact-management.bwhslep.mongodb.net/users')
.then(()=> app.listen(8000));