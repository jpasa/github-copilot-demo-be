const express = require('express');
const userRouter = express.Router();
const { displayUsers, createUser, editUser, editUserForm, deleteUser } = require('../controllers/user-controller')

userRouter.get('/', displayUsers);
userRouter.post('/create-user', createUser);
userRouter.put('/edit-user/:id', editUser)
userRouter.get('/edit-user/:id',editUserForm);
userRouter.get('/delete-user/:id', deleteUser);

module.exports = userRouter;