const User = require('../models/user');

//displays users from the db to home page order by recently added as first item in the list
const displayUsers = async (req, res) => {
    const users = await User.find().sort({ createdAt: -1 });
    res.send({ users });
};

//adds user to the db
const createUser = async (req, res) => {
    const body = req.body;
    const payload = {
        "firstname": body.firstname,
        "lastname": body.lastname,
        "physical_address": body.physical_address,
        "billing_address": body.billing_address
    }
    const user = await User.create(payload);
    res.send(user);
};
//displays update user form
const editUserForm = async (req, res) => {
    try {
        const id = req.params.id;
        const user = await User.findById(id);
        res.send({user});
    } catch (err) {
        res.send({err}); 
    }
};

//updates selected user in db
const editUser = async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    const payload = {
        "firstname": req.body.firstname,
        "lastname": req.body.lastname,
        "physical_address": body.physical_address,
        "billing_address": body.billing_address
    }
    try {
        await User.findByIdAndUpdate(id, payload);
        res.send({ redirect: '/' })
    } catch (err) {
        res.send({err});
    }
};

//deletes selected user in db
const deleteUser = async (req, res) => {
    const id = req.params.id;
    try {
        await User.findByIdAndDelete(id);
        res.send({ redirect: '/' })
    } catch (err) {
        res.send({err});
    }
};

module.exports = { displayUsers, createUser, editUser, editUserForm, deleteUser };