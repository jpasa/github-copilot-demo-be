const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    physical_address: { type: String, required: true },
    billing_address: { type: String, required: true }
}, { timestamps: true });

const User = mongoose.model('user', userSchema);

module.exports = User;