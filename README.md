# User Contact Management System

User Contact Management System is a Node.js application that is where you are able to manage users contact information. Where you can create, read, update and delete users from MongoDB. 

## Requirements
Before you begin make sure your development environment includes [Node.js](https://nodejs.org/en/download/) and NPM.

## Local Development
- Clone the project either using SSH or HTTPS. 
- Run a bash/cmd terminal and run the installation command `npm install`
- Run `npm start`